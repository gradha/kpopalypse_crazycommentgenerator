
################################################################################
#                                                                              #
# Hi, it's gradha here - thanks for checking this Nim rewrite of Kpopalypse    #
# Python thingy. Checking out his Patreon and helping him in his eternal quest #
# to be the k-pop blogger with maximum determination!                          #
#                                                                              #
# This is the source code for the Kpopalypse fan comment generator. It was     #
# rewritten by gradha to be able to compile under Nim 0.19.4, a highly         #
# determined caonima of a language, stylistically quite close to Python.       #
#                                                                              #
# Kpopalypse is not very good at this programming thing, and this initial Nim  #
# translation tries to stay close to the original version to highlight         #
# similarities and differences between both programming languages. These       #
# comments will talk you through each step of the code if you're a know-nothing#
# about programming like Kpopalypse.                                           #
#                                                                              #
# Enjoy and  don't forget to visit kpopalypse.com for all your caonima needs!  #
#                                                                              #
################################################################################


################################################################################
#
# Both Nim and Python use the keyword `import` to include extra code and call
# it. By default Nim already does import a few behind the scenes, so there is
# no need to write `import system`, that is usually imported already.
#

import os

# Kpopalypse likes his terminal at 159 columns, but crummy old programmers
# prefer 80 columns as default width, so that they can sneer at all the _young_
# coders with fancy ultra widescreen monitors, while secretly wishing they were
# cool and hip as them.
#
# Also, the command only works on Windows, so other platforms will just spit an
# error on the standard output and keep with whatever size you prefer.
#

discard execShellCmd "mode con: cols=159 lines=40"

#
# In Python you can declare globals anywhere. In Nim you need to declare them
# first, which we will do here through var statements.

var
  isare: string
  gender: string
  gendercap: string
  m1: string
  m4: string
  m2: string
  m5: string
  m3: string
  m7: string
  m6: string
  m8: string
  m9: string
  m10: string
  group: string
  adj1: string
  adj2: string
  adj3: string

# The program runs as a series of modules, (that's what Kpopalypse calls a
# function or procedure) each module is first defined by the "proc" statement
# (def in Python). Unlike Python, Nim procs need to be _known_ to the compiler
# before they can be called, so we need to forward declare them here, or
# reorder all the _modules_, which would make comparisons against the Python
# version harder to follow.
#
################################################################################
#
#

proc boygirlgroup()
proc boygirlsolo()
proc solo()
proc grouproutine()
proc adjectives()
proc printout()
proc program()

# In Nim there is no `input()` proc, instead `readline(stdin)` is used. But we
# can create our own `input()` proc, and that will automatically make all the
# Python code Nim functional! Same with the `echo()` proc, from now
# on we will use `print` like Python does.
#
# Note how in Nim you don't necessarily `return` values, you can assign them to
# the magical `result` variable, and that's what will be returned to the
# caller.

proc input(): string =
  result = readLine(stdin)

# Same like with `input()`, Nim doesn't have any `print()` proc, so we create
# it here writing all the input variable arguments to the standard output.
proc print[T](texts: varargs[T, `$`]) =
  for x in texts: write(stdout, x)
  echo ""

proc multiples() =
  print "Are you stanning a group or a solo performer?"
  print "Press g for group, or s for a solo performer."
  print "Press ENTER after your selection."

#
# The program asks for keyboard input and makes the result a variable called
# "selectoneofmany".
#

  var selectoneofmany = input()

#
# The result determines what grammar is used, and also where the program goes
# next.  If you're stanning a group then the module that runs next is called
# boygirlgroup(), or if it's a solo performer the program goes to
# boygirlsolo().
#

  if selectoneofmany == "g":
    isare = "are"
    gender = "their"
    gendercap = "Their"
    boygirlgroup()
  elif selectoneofmany == "s":
    isare = "is"
    boygirlsolo()

#
# If you're a complete twit who can't type, this "else" statement catches your
# invalid input, And returns you to the top of the module called multiples().
# (gradha: caonimas with high determination levels can keep failing and
# overflow the program stack, crashing everything!)
#

  else:
    print "Please enter a valid selection."
    print ""
    multiples()

#
#
################################################################################
#
#

#
# If you select a group but only select one group member, the program will
# sense that you are trolling and ask you to reflect.  If you say yes the
# program runs again from the start, otherwise you are "cancelled and
# dismissed" and the program terminates.
#

proc reflection() =
  print "Stop trying to break things!"
  print "Will you reflect and return with a more mature image? (y/n)"
  var reflect = input()
  if reflect == "y":
    program()
  else:
    quit(0)

#
#
################################################################################
#
#

#
# The program asks the gender of the solo performer.  This is needed for
# grammar purposes.
#


proc boygirlsolo() =
  print "Is the performer male or female?"
  print "Press m for male, or f for female."
  print "Press ENTER after your selection."
  var maleorfemale = input()
  if maleorfemale == "m":
    gender = "his"
    gendercap = "His"
  elif maleorfemale == "f":
    gender = "her"
    gendercap = "Her"

#
# If you try and write something fancy like "attack helicopter" you will be
# told that there are only two genders, and then this section of the program
# restarts.  If this is offensive to your gender identity, replace the below
# lines with this:
#
#   else:
#     gender = "ze"
#     gendercap = "Ze"
#

  else:
    print "THERE ARE ONLY TWO GENDERS!!!!!!!11!1!1111oneoneeleven"
    print "Please enter a valid selection."
    print ""
    boygirlsolo()

#
# Once the gender is determined, then the module solo() runs.
#

  solo()

#
#
################################################################################
#
#

proc boygirlgroup() =

#
# This section is similar to the gender selection above except here a third
# gender IS allowed because of the posibility of co-ed groups.
#

  print "What gender is the group?"
  print "Press m for male, f for female or c for co-ed."
  print "Press ENTER after your selection."
  var maleorfemale = input()
  if maleorfemale == "m":
    gender = "his"
    gendercap = "Their"
  elif maleorfemale == "f":
    gender = "her"
    gendercap = "Their"
  elif maleorfemale == "c":
    gender = "their"
    gendercap = "Their"
  else:
    print "THERE ARE ONLY TWO GENDERS!!!!!!!11!1!1111oneoneeleven"
    print "Please enter a valid selection."
    print ""
    boygirlgroup()

#
# Once we've settled the gender war, the module grouproutine() runs for groups.
#

  grouproutine()

#
#
################################################################################
#
#

#
# The solo() section asks for the name of the performer and throws it into
# every single space in the final text where a name is needed.
#

proc solo() =
  print "Enter the name of your solo performer."

#
# In this part Kpopalypse would define more globals to hold names. Since Nim
# required us to put them once in the beginning, we can just use them.
#
# This code makes every member the same name as the first member.
#

  m1 = input()
  m2 = m1
  m3 = m1
  m4 = m1
  m5 = m1
  m6 = m1
  m7 = m1
  m8 = m1
  m9 = m1
  m10 = m1
  group = m1

#
# The adjectives module is then run.
#

  adjectives()

#
#
################################################################################
#
#

proc grouproutine() =

  while true:

#
# This whole "while True" business is kinda lame, but the idea of it is that it
# puts everything below it in a loop, which means I can then use "break"
# statements to exit that loop if I want, this is convenient from a program
# flow perspective.
#

    print "Enter the name of the group."
    group = input()
    print "Enter the name of any group member."

    m1 = input()
    print "Enter the name of another group member."

    m2 = input()

#
# This is the bit where if you don't have more than one member in your group
# the program thinks you're being a massive fuckwad and puts you in reflection.
#

    if m2 == "":
      print "I thought you said this was a group, not a solo performer!"
      reflection()

    print "Enter the name of another group member."
    print "If there are no more members, just press ENTER."

#
# The rest of this section is all the same - it's assigning different variables
# to the different group members you input.  If you have less than ten members,
# the program just recycles members you used earlier, in order.  Note that
# while the program can store a maximum of ten members, only nine members are
# used in the final text.  This is because I forgot how many members were in
# SF9 (because I was too much of a dumb fuck to realise what the 9 in SF9
# meant) so I just guessed that there might have been ten and wrote the code
# around that, then couldn't be fucked changing it once I realised.  If you
# press Enter without giving a name, the program breaks out of the loop
# established earlier so it can move onto the next thing.
#

    m3 = input()
    if m3 == "":
      m3 = m1
      m4 = m2
      m5 = m1
      m6 = m2
      m7 = m1
      m8 = m2
      m9 = m1
      m10 = m2
      break
    print "Enter the name of another group member."
    print "If there are no more members, just press ENTER."

    m4 = input()
    if m4 == "":
      m4 = m1
      m5 = m2
      m6 = m3
      m7 = m1
      m8 = m2
      m9 = m3
      m10 = m1
      break
    print "Enter the name of another group member."
    print "If there are no more members, just press ENTER."

    m5 = input()
    if m5 == "":
      m5 = m1
      m6 = m2
      m7 = m3
      m8 = m4
      m9 = m1
      m10 = m2
      break
    print "Enter the name of another group member."
    print "If there are no more members, just press ENTER."

    m6 = input()
    if m6 == "":
      m6 = m1
      m7 = m2
      m8 = m3
      m9 = m4
      m10 = m5
      break
    print "Enter the name of another group member."
    print "If there are no more members, just press ENTER."

    m7 = input()
    if m7 == "":
      m7 = m1
      m8 = m2
      m9 = m3
      m10 = m4
      break
    print "Enter the name of another group member."
    print "If there are no more members, just press ENTER."

    m8 = input()
    if m8 == "":
      m8 = m1
      m9 = m2
      m10 = m3
      break
    print "Enter the name of another group member."
    print "If there are no more members, just press ENTER."

    m9 = input()
    if m9 == "":
      m9 = m1
      m10 = m2
      break
    print "Enter the name of another group member."
    print "If there are no more members, just press ENTER."

    m10 = input()
    if m10 == "":
      m10 = m1
      break
    print "Okay, that's enough members.  Nobody cares about the other ones."
    break

#
# Once the program breaks, the adjectives routine is run, which means that our
# "solo" and "group" program flows now meet up here.
#

  adjectives()

#
#
################################################################################
#
#

#
# This code asks some basic questions and substitutes them for certain
# adjectives in the original fan email.  If you want it to read exactly like
# the original, the three adjectives are "deep", "beautiful" and "incredible".
# Of course "deep" doesn't really match food in any way, but I thought the
# results were funnier when I used food as the element to describe.  The
# program will accept any input here, or even none at all.
#

proc adjectives() =
  print ""
  print "In one word only, describe the taste of your favourite food."
  adj1 = input()
  print "In one word only, describe the appearance of someone you are physically attracted to."
  adj2 = input()
  print "In one word only, describe the feeling of a (good) orgasm."
  adj3 = input()

#
# The printout is the program's payoff.
#

  printout()

#
#
################################################################################
#
#

#
# It should be fairly obvious how this works - the variable names defined
# earlier are called and replace certain key words in the original text.
#

proc printout()=
  print ""
  print "Your crazy k-pop fan comment copypasta is below.  Please feel free to copy and paste, use anywhere - especially on www.asianjunkie.com!"
  print ""
  print "-------------------------------------------------------------------"
  print ""
  print "I see that everyone is following the pack."
  print "Someone has an opinion and all of a sudden it is fact."
  print group, " ", isare, " absolutely unique in the industry. ",
    gendercap, " chemistry is ", adj2, ", ", gender,
    " vocals are distinctive and so well arranged!"
  print "I was more impressed that ", m1, " started this ", adj2,
    " arrangement off and the progression through ", m2, ", ", m3,
    " and ", m4, " with that rich tone of ", m5, " was beautifully"
  print "orchestrated into the chorus into a ", adj2,
    " blend of vocals with ", m6, " breaking through ", gender, " ",
    adj2, " range! We have ", m7, " singing through at key moments!"
  print "Then comes the ", adj1, " and ", adj2, " voices of ", m8,
    " and ", m9, "."
  print ""
  print "The song is a very delicious recipe of vocals that draws you in and highlights the individuality of each member!"
  print group, " always ", isare, " uniquely talented and outstanding."
  print "Listen to it with more knowledge and pay attention to the tones and blends of some of the most ",
    adj2, " vocals in the industry. They are ", adj3, " artists and ",
    "this was a song highlighting ", gender, " uniqueness."
  print ""
  print "Just because someone wrote this review doesn't make it true. It is an opinion and an opinion that lacks knowledge of these artists and music!"
  print ""
  print group, " will always stand out. Wait and see what's coming next!"
  print ""
  print "Beautiful work ", group, ". Congratulations !"
  print ""
  print "-------------------------------------------------------------------"
  print ""

#
# If I don't have a "run this again" prompt, the program immediately exits after creating the
# printout, which means that you don't get time to read any of it.
#

  print "Run the program again? (y/n)"
  var yesno = input()
  if yesno == "y":
    program()
  else:
    quit()

#
#
################################################################################
#
#

#
# This routine marks the start of the program, and then moves to the multiples
# module, which was the first one programmed.
#

proc program() =
  print ""
  print "-------------------------------------------------------------------"
  print "||                                                               ||"
  print "||          K-pop fan comment generator, by Kpopalypse           ||"
  print "||                                                               ||"
  print "-------------------------------------------------------------------"
  print ""
  multiples()

#
#
################################################################################
#
#

#
# Now that all the routines are defined, the following line kicks off the
# program itself.  Because every module either exits the program or sends the
# program to the start of another module once it finishes, it doesn't matter
# that the routines are not in any strict order - the point of writing the
# program this way is to have the ability to jump between routines at will.
#

program()

#
#
################################################################################

################################################################################
#                                                                              #
# That's it, caonima!  Hopefully this was vaguely interesting for you!         # 
# Kpopalypse will return with more content!  Stay determined!                  #
#                                                                              #
################################################################################
