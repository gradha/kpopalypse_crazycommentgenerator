# Kpopalypse crazy comment generator translated to nim webapp with Karax.
#
# This is a version of the file found in the 02_nim_command_line directory but
# adapted to being a webapp. Since string substitution is pretty simple, the
# 99% of this version deals with sort of emulating a console like interface in
# a web page.

# The strutils module is imported for methods like toLower to make sure that
# user input is not capitalized, which happens by default on most mobile web
# browsers.
import strutils, macros

# Karax is included directly. For some reason karax examples do this instead of
# import, so we do the same.
include karax / prelude

## To simulate a changing web page, we need to store the 'state' of where the
## user is in the whole process flow. Each step is identified by a different
## state, and we check this state to know how to 'render' our web page.
type
  State = enum
    sStart, sStanningSolo, sStanningGroup, sSoloName, sAdjective1,
    sAdjective2, sAdjective3, sResult, sGroupName, sGroupMember1,
    sGroupMember2, sGroupMember3, sGroupMember4, sGroupMember5,
    sGroupMember6, sGroupMember7, sGroupMember8, sGroupMember9, sGroupMember10


# These constants are declared here for easier use later and avoid typing
# errors (if you wrote strings like "__blank" or "blank" directly, the compiler
# would not mind, but if you type incorrectly the name of a variable it will.
const
  blank = "_blank"
  inputFieldId = "inputFieldId"
  url_kpopalypse = "https://kpopalypse.com/2019/02/24/the-kpopalypse-fan-comment-generator/"
  url_asianjunie = "http://www.asianjunkie.com"
  url_source = "https://gitlab.com/gradha/kpopalypse_crazycommentgenerator"


# The history global variable will be our console emulation. It will hold all
# the lines of text produced so far, and will be rendered each time the web
# page changes.
var history: seq[kstring] = @[]

# The global variable keeping were we are in the state, starting at sStart.
var screen = sStart

# A global variable containing the error string that will optionally be
# presented to the users with bad input. It is actually shown always, but since
# it is blank, nobody notices until it is filled with an error.
var errorMessage = kstring""

# Finally, here are the 'normal' variables we carry from the command line
# version. They work the same, but use the kstring type instead of string, as
# specified by the Karax framework for html output.
var
  isare: kstring
  gender: kstring
  gendercap: kstring
  m1: kstring
  m4: kstring
  m2: kstring
  m5: kstring
  m3: kstring
  m7: kstring
  m6: kstring
  m8: kstring
  m9: kstring
  m10: kstring
  group: kstring
  adj1: kstring
  adj2: kstring
  adj3: kstring


# The strutils module provides a toLower() proc for strings. We adapt it here
# to work with kstrings, converting the kstring first to a string, then lower
# it, and finally back to a kstring.
proc toLower(x: kstring): kstring =
  let text = $x
  result = kstring(text.toLower)


# This proc will generate a sequence of kstrings containing the final text
# output using the global variables accumulated so far. It is used for both the
# html output and the clipboard copying.
proc generateResult(): seq[kstring] =
  result = @[]
  result.add "I see that everyone is following the pack."
  result.add "Someone has an opinion and all of a sudden it is fact."
  result.add (group & " " & isare & " absolutely unique in the industry. " &
    gendercap & " chemistry is " & adj2 & ", " & gender &
    " vocals are distinctive and so well arranged!")
  result.add ("I was more impressed that " & m1 & " started this " & adj2 &
    " arrangement off and the progression through " & m2 & ", " & m3 &
    " and " & m4 & " with that rich tone of " & m5 & " was beautifully" &
    "orchestrated into the chorus into a " & adj2 &
    " blend of vocals with " & m6 & " breaking through " & gender & " " &
    adj2 & " range! We have " & m7 & " singing through at key moments!")
  result.add ("Then comes the " & adj1 & " and " & adj2 & " voices of " & m8 &
    " and " & m9 & ".")
  result.add ""
  result.add ("The song is a very delicious recipe of vocals that draws " &
    "you in and highlights the individuality of each member!")
  result.add group & " always " & isare & " uniquely talented and outstanding."
  result.add ("Listen to it with more knowledge and pay attention to the " &
    "tones and blends of some of the most " &
    adj2 & " vocals in the industry. They are " & adj3 & " artists and " &
    "this was a song highlighting " & gender & " uniqueness.")
  result.add ""
  result.add ("Just because someone wrote this review doesn't make it true. " &
    "It is an opinion and an opinion that lacks knowledge of these artists " &
    "and music!")
  result.add ""
  result.add group & " will always stand out. Wait and see what's coming next!"
  result.add ""
  result.add "Beautiful work " & group & ". Congratulations !"


# This is a web emulation of the console input() proc. Given the the default
# inputfield, we extract the text and return it. If there is something wrong,
# we populate the errorMessage variable and return the empty string by default.
proc getInput(): kstring =
  let inputField = getVNodeById(inputFieldId)
  if inputField.isNil or inputField.text == "":
    errorMessage = "Please type something"
    result = ""
  else:
    errorMessage = ""
    result = inputField.text
    inputField.setInputText ""


# In order to set an adjective and go to the next screen, we need to create a
# proc for each variable, which will be used as callbacks in the appropriate
# buttons. Nim provides highly advanced templating features which allow us to
# write a generic template and substitute parts of it. Here we have the basic
# skeleton of a proc setting an adjective. We only have to write it once and
# let the compiler replace the parts we want to change.
template setAdjectiveN(procName, adjectiveVar, nextState, extraLine): untyped =
  proc procName() =
    let input = getInput()
    if input.len > 0:
      history.add input
      if extraLine.len > 0:
        history.add $extraLine
      adjectiveVar = input.toLower
      screen = nextState


# And here we *instantiate* three different procs using the template above.
setAdjectiveN(setAdjective3, adj3, sResult, "Finished")
setAdjectiveN(setAdjective2, adj2, sAdjective3, "")
setAdjectiveN(setAdjective1, adj1, sAdjective2, "")


# We repeat the same template magic for all the variables setting the group
# member variables, each will have a numbered proc that will be later used as a
# callback.
template setGroupMemberN(procName, nameVar, nextState, extraLine): untyped =
  proc procName() =
    let input = getInput()
    if input.len > 0:
      history.add input
      if extraLine.len > 0:
        history.add extraLine
      nameVar = input
      screen = nextState


setGroupMemberN(setGroupMember10, m10, sAdjective1,
    "Okay, that's enough members. Nobody cares about the other ones")

setGroupMemberN(setGroupMember9, m9, sGroupMember10, "")
setGroupMemberN(setGroupMember8, m8, sGroupMember9, "")
setGroupMemberN(setGroupMember7, m7, sGroupMember8, "")
setGroupMemberN(setGroupMember6, m6, sGroupMember7, "")
setGroupMemberN(setGroupMember5, m5, sGroupMember6, "")
setGroupMemberN(setGroupMember4, m4, sGroupMember5, "")
setGroupMemberN(setGroupMember3, m3, sGroupMember4, "")
setGroupMemberN(setGroupMember2, m2, sGroupMember3, "")
setGroupMemberN(setGroupMember1, m1, sGroupMember2, "")


# Sets the group name and changes to the first sGroupMember1 state.
proc setGroupName() =
  let input = getInput()
  if input.len > 0:
    history.add "Group name " & input
    group = input
    screen = sGroupMember1


# Sets the solo name and changes to the first sAdjective1 state.
proc setSoloName() =
  let input = getInput()
  if input.len > 0:
    history.add "Solo performer " & input
    m1 = input
    m2 = m1
    m3 = m1
    m4 = m1
    m5 = m1
    m6 = m1
    m7 = m1
    m8 = m1
    m9 = m1
    m10 = m1
    group = m1
    screen = sAdjective1


# This is javascript code that I copied directly from
# https://stackoverflow.com/a/33928558/172690 as javascript. I could not figure
# out how to write this in Nim, and it seemed a waste to translate it to Nim so
# that Nim could later generate its javascript version…
#
# One can usually put such javascript files in external files and reference
# them, but I wanted to create a webapp contained in a single file, hence the
# use of the `emit` compiler pragma which bypasses Nim code and generates
# javascript directly.
proc copyResultToClipboard() =
  # Prepare a variable with the contents of the whole result.
  var fullText = kstring""
  for line in generateResult():
    fullText.add line & "\n"

  # Use the variable fullText inside the generated javascript code.
  {.emit: """
if (window.clipboardData && window.clipboardData.setData) {
  // IE specific code path to prevent textarea being shown while dialog is visible.
  clipboardData.setData("Text", `fullText`);

} else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
  var textarea = document.createElement("textarea");
  textarea.textContent = `fullText`;
  textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
  document.body.appendChild(textarea);
  textarea.select();
  try {
    document.execCommand("copy");  // Security exception may be thrown by some browsers.
    alert("Text copied, now go paste it somewhere!");
  } catch (ex) {
    alert("Copy to clipboard failed :-(", ex);
  } finally {
    document.body.removeChild(textarea);
  }
}
""".}


# Generates the final output of the crazy comment generator. This calls the
# generateResult() proc we declared above and translates each line to an HTML
# div or p DOM element.
proc buildResult(): VNode =
  result = buildHtml(tdiv):
    p: discard
    tdiv:
      text """
Your crazy k-pop fan comment copypasta is below.  Please feel free to copy and
paste, use anywhere - especially on
"""
      a(target = blank, href = url_asianjunie):
        text url_asianjunie
      text "!"
    p: discard
    button:
      text "Copy text below to clipboard"
      proc onclick(ev: Event; n: VNode) =
        copyResultToClipboard()
    text "(if you don't see a success popup you will need to copy it manually)"
    hr: discard
    for line in generateResult():
      if line.len > 0:
        tdiv: text line
      else:
        p: discard


# Single questions about adjectives. These procs were small and different
# enough that making a template out of them would be more confusing than
# useful. So I decided to leave them as individual procs.
proc buildAdjective1(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "In one word only, describe the taste of your favourite food."
    input(class = "input", id = inputFieldId, onkeyupenter = setAdjective1)
    tdiv: text errorMessage


proc buildAdjective2(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "In one word only, describe the appearance of someone you " &
        "are physically attracted to."
    input(class = "input", id = inputFieldId, onkeyupenter = setAdjective2)
    tdiv: text errorMessage


proc buildAdjective3(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "In one word only, describe the feeling of a (good) orgasm."
    input(class = "input", id = inputFieldId, onkeyupenter = setAdjective3)
    tdiv: text errorMessage


proc buildSoloName(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of your solo performer: "
    input(class = "input", id = inputFieldId, onkeyupenter = setSoloName)
    tdiv: text errorMessage


# Cleanup function like in the original program, to cater for variable size
# groups copies remaining name slots using the filled ones as source.
proc setRemainingGroupMembers(member: int) =
  case member:
    of 3:
      m3 = m1
      m4 = m2
      m5 = m1
      m6 = m2
      m7 = m1
      m8 = m2
      m9 = m1
      m10 = m2
    of 4:
      m4 = m1
      m5 = m2
      m6 = m3
      m7 = m1
      m8 = m2
      m9 = m3
      m10 = m1
    of 5:
      m5 = m1
      m6 = m2
      m7 = m3
      m8 = m4
      m9 = m1
      m10 = m2
    of 6:
      m6 = m1
      m7 = m2
      m8 = m3
      m9 = m4
      m10 = m5
    of 7:
      m7 = m1
      m8 = m2
      m9 = m3
      m10 = m4
    of 8:
      m8 = m1
      m9 = m2
      m10 = m3
    of 9:
      m9 = m1
      m10 = m2
    of 10:
      m10 = m1
    else:
      discard


# The following template generates the HTML DOM for each member screen step.  I
# was planning on creating a template similar to the ones above, but for some
# reason the compiler was complaining about unexpected nodes! Unfortunately
# this sometimes happens during metaprogramming, you risk reaching the limits
# of the compiler and finding code generation bugs! In this case, the compiler
# seemed to not like the first `tdiv` with 'Error: Expected a node of kind
# nnkIdent, got nnkOpenSymChoice'. So lets leave individual procs here for the
# time being…

proc buildGroupMember10(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember10)
    button:
      text "Click here if there are no more members"
      proc onclick(ev: Event; n: VNode) =
        setRemainingGroupMembers(10)
        screen = sAdjective1
    tdiv: text errorMessage


proc buildGroupMember9(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember9)
    button:
      text "Click here if there are no more members"
      proc onclick(ev: Event; n: VNode) =
        setRemainingGroupMembers(9)
        screen = sAdjective1
    tdiv: text errorMessage


proc buildGroupMember8(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember8)
    button:
      text "Click here if there are no more members"
      proc onclick(ev: Event; n: VNode) =
        setRemainingGroupMembers(8)
        screen = sAdjective1
    tdiv: text errorMessage


proc buildGroupMember7(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember7)
    button:
      text "Click here if there are no more members"
      proc onclick(ev: Event; n: VNode) =
        setRemainingGroupMembers(7)
        screen = sAdjective1
    tdiv: text errorMessage


proc buildGroupMember6(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember6)
    button:
      text "Click here if there are no more members"
      proc onclick(ev: Event; n: VNode) =
        setRemainingGroupMembers(6)
        screen = sAdjective1
    tdiv: text errorMessage


proc buildGroupMember5(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember5)
    button:
      text "Click here if there are no more members"
      proc onclick(ev: Event; n: VNode) =
        setRemainingGroupMembers(5)
        screen = sAdjective1
    tdiv: text errorMessage


proc buildGroupMember4(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember4)
    button:
      text "Click here if there are no more members"
      proc onclick(ev: Event; n: VNode) =
        setRemainingGroupMembers(4)
        screen = sAdjective1
    tdiv: text errorMessage


proc buildGroupMember3(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember3)
    button:
      text "Click here if there are no more members"
      proc onclick(ev: Event; n: VNode) =
        setRemainingGroupMembers(3)
        screen = sAdjective1
    tdiv: text errorMessage


proc buildGroupMember2(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of another group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember2)
    tdiv: text errorMessage


proc buildGroupMember1(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of any group member: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupMember1)
    tdiv: text errorMessage


proc buildGroupName(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Enter the name of the group: "
    input(class = "input", id = inputFieldId, onkeyupenter = setGroupName)
    tdiv: text errorMessage


proc buildStanningGroup(): VNode =
  result = buildHtml(tdiv):
    p: discard
    button:
      text "It's a male group"
      proc onclick(ev: Event; n: VNode) =
        history.add "It's a male group"
        gender = "his"
        gendercap = "Their"
        screen = sGroupName

    button:
      text "It's a female group"
      proc onclick(ev: Event; n: VNode) =
        history.add "It's a female group"
        gender = "her"
        gendercap = "Their"
        screen = sGroupName

    button:
      text "It's a co-ed group"
      proc onclick(ev: Event; n: VNode) =
        history.add "It's a co-ed group"
        gender = "their"
        gendercap = "Their"
        screen = sGroupName


proc buildStanningSolo(): VNode =
  result = buildHtml(tdiv):
    p: discard
    button:
      text "It's a male performer"
      proc onclick(ev: Event; n: VNode) =
        history.add "It's a male performer"
        gender = "his"
        gendercap = "His"
        screen = sSoloName

    button:
      text "It's a female performer"
      proc onclick(ev: Event; n: VNode) =
        history.add "It's a female performer"
        gender = "her"
        gendercap = "Her"
        screen = sSoloName


proc buildStart(): VNode =
  result = buildHtml(tdiv):
    tdiv: text "Are you stanning a group or a solo performer?"
    p: discard

    button:
      text "I'm stanning a group"
      proc onclick(ev: Event; n: VNode) =
        history.add "Stanning a group"
        isare = "are"
        gender = "their"
        gendercap = "Their"
        screen = sStanningGroup
        redraw()

    button:
      text "I'm stanning a solo performer"
      proc onclick(ev: Event; n: VNode) =
        history.add "Stanning a solo performer"
        isare = "is"
        screen = sStanningSolo
        redraw()


proc resetState() =
  screen = sStart
  history = @[]


# This is our actual page renderer. It takes all the state stored in the global
# variables and depending on the screen generates a different DOM subtree,
# representing different states of the web page.
proc createDom(): VNode =
  result = buildHtml(tdiv):
    h1:
      text "K-pop fan comment generator, by Kpopalypse"
    tdiv:
      text "For context on the original comment generator "
      a(target = blank, href = url_kpopalypse):
        text "follow this link"
      text ". Source code "
      a(targat = blank, href = url_source):
        text "here"
      text "."

    if history.len > 0:
      p: discard
      button:
        text "Click to reflect and return with a more mature image"
        proc onclick(ev: Event; n: VNode) =
          resetState()

    p: discard
    tdiv:
      for x in history:
        text x & ". "

    case screen:
      of sStart: buildStart()
      of sStanningSolo: buildStanningSolo()
      of sStanningGroup: buildStanningGroup()
      of sSoloName: buildSoloName()
      of sGroupName: buildGroupName()
      of sGroupMember1: buildGroupMember1()
      of sGroupMember2: buildGroupMember2()
      of sGroupMember3: buildGroupMember3()
      of sGroupMember4: buildGroupMember4()
      of sGroupMember5: buildGroupMember5()
      of sGroupMember6: buildGroupMember6()
      of sGroupMember7: buildGroupMember7()
      of sGroupMember8: buildGroupMember8()
      of sGroupMember9: buildGroupMember9()
      of sGroupMember10: buildGroupMember10()
      of sAdjective1: buildAdjective1()
      of sAdjective2: buildAdjective2()
      of sAdjective3: buildAdjective3()
      of sResult: buildResult()


setRenderer createDom
