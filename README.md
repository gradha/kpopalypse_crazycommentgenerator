# Kpopalypse crazycommentgenerator

On the 24th of February of 2019, [Kpopalypse released a
crazycommentgenerator](https://kpopalypse.com/2019/02/24/the-kpopalypse-fan-comment-generator/)
as a Windows binary. Implemented in Python 3, the source can be found in
[01_original_python](01_original_python).

I first translated it to the [Nim programming language](https://nim-lang.org),
you can see the results in [02_nim_command_line](02_nim_command_line).

Then I added a webapp version using
[Karax](https://github.com/pragmagic/karax), which you can check out in
[03_nim_webapp](03_nim_webapp).

This source code uses [git
submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules), so you need
to either check it out using git's clone `--recursive` switch, or run `git
submodule update` from inside the directory once you have it.


## License

[MIT License](LICENSE.md).
